const jwt = require('jsonwebtoken');
const mongoose = require('mongoose');
const asyncHandler = require('express-async-handler');
const User = require('../models/userModel')

// Define the middleware function 'verified'
const verified = asyncHandler(async(req, res, next) => {
    let token
    // Check if the 'authorization' header exists and starts with 'Bearer'
    if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')){
        try {
            // Get token from the 'authorization' header
            token = req.headers.authorization.split(' ')[1]
            
            // Verify the token using the SECRET from environment variables
            const decoded = jwt.verify(token, process.env.SECRET)

            // Get user from the token using the User model
            req.user = await User.findById(mongoose.Types.ObjectId(decoded.id)).select('-password')


            // Call the next middleware
            next()
        } catch (error) {
            console.log(error)
            res.status(401)
            throw new Error('Not authorized')
        }
    }

    // If there is no token, return a 401 error
    if (!token) {
        res.status(401)
        throw new Error('Not authorized, no token.')
    }
})

const onlyAdmin = asyncHandler(async(req, res, next) => {
    let token
    // Check if the 'authorization' header exists and starts with 'Bearer'
    if(req.headers.authorization && req.headers.authorization.startsWith('Bearer')){
        try {
            // Get token from the 'authorization' header
            token = req.headers.authorization.split(' ')[1]
            
            // Verify the token using the SECRET from environment variables
            const decoded = jwt.verify(token, process.env.SECRET)

            // Get user from the token using the User model
            req.user = await User.findById(mongoose.Types.ObjectId(decoded.userId)).select('-password')

            if(!req.user) {
                res.status(401).json({ message: "Unauthorized, user not found" });
            }
            // Check if user is an admin
            else if(req.user.isAdmin === true) {
                // Call the next middleware
                next()
            } else {
                res.status(403).json({ message: "Forbidden" });
            }
        } catch (error) {
            console.log(error)
            res.status(401)
            throw new Error('Not authorized')
        }
    }

    // If there is no token, return a 401 error
    if (!token) {
        res.status(401)
        throw new Error('Not authorized, no token.')
    }
});

const decode = (token) => {
    if (typeof token != "undefined") {
        console.log(token);

        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (err, data) => {

            if (err) {
                return null;
            } else {

                return jwt.decode(token, { complete: true }).payload;

            }

        })
    } else {
        return null;
    }
}


// Export the 'verified' function
module.exports = { verified, onlyAdmin, decode }
