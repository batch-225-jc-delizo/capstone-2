const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderController');
const onlyAdmin = require('../middleware/authMiddleware').onlyAdmin;
const verified = require('../middleware/authMiddleware').verified
const auth = require('../middleware/auth');

// Check Out, Create an Order
router.post('/checkout', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    if (!user) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    orderController.checkout({ ...req, user }, res);
});
// Retrieve all Orders of a User
router.get('/retrieve-a-user-order', auth.verify, (req, res) => {
    // console.log('route hit');
    const user = auth.decode(req.headers.authorization);
    if (!user) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    orderController.getOrdersOfUser({ ...req, user }, res);
});




// Retrieve all Orders of all the Users
router.get('/get-orders', orderController.getOrdersOfAllUsers);




// Delete an Order
router.delete('/delete-order/:id', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    if (!user) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    orderController.deleteOrder({ ...req, user }, res);
});

// Order Delivered
router.put('/order-delivered/:id', (req, res) => {
  const user = auth.decode(req.headers.authorization);
  if (!user) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
  orderController.orderDeliver({ ...req, user }, res);
});


// Filter Delivered Orders
router.get('/delivered', (req, res) => {
  const user = auth.decode(req.headers.authorization);
  if (!user) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
  orderController.delivered({ ...req, user }, res);
});

// Filter Not Delivered Orders
router.get('/not-delivered', (req, res) => {
  const user = auth.decode(req.headers.authorization);
  if (!user) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
  orderController.notDelivered({ ...req, user }, res);
});






module.exports = router;
