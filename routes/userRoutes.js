const express = require('express')
const router = express.Router()

const userController = require('../controllers/userController');
const { verified, onlyAdmin, decode } = require('../middleware/authMiddleware');
const auth = require('../middleware/auth');

// REGISTER ROUTE
router.post('/register', (req, res) => {
    userController.registerUser(req, res);
});
// LOGIN ROUTE
// router.post('/login', (req, res) => {
//     userController.loginUser(req, res)
//         .then((result) => {
//             res.json(result);
//         })
//         .catch((error) => {
//             res.status(500).json({ error: error.message });
//         });
// });
// Route for user authentication
router.post("/login", (req, res) => {
  userController.loginUser(req.body).then(result => res.send(result));
})

// GET ANY USER DETAILS (Admin Only)
router.get('/whosthisuser/:id', auth.verify, (req, res) => {
  const user = auth.decode(req.headers.authorization);
  if (!user) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
  userController.getUser({ ...req, user }, res);
});

// GET ALL USERS (Admin Only)
router.get('/all', auth.verify, (req, res) => {
  const user = auth.decode(req.headers.authorization);
  if (!user) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
  userController.getAllUsers({ ...req, user }, res);
});

// RETRIEVE A USER'S OWN DETAILS
router.get('/me', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization)
    userController.meUser(userData).then(resultFromController => res.send(resultFromController));
  })
  
// PROMOTE A USER TO ADMIN
router.put('/makeadmin/:id', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (!userData || !userData.isAdmin) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
    userController.promoteToAdmin({ ...req, userData }, res);
});

// DEMOTE AN ADMIN TO USER
router.put('/makeuser/:id', auth.verify, (req, res) => {
  const userData = auth.decode(req.headers.authorization);
  if (!userData || !userData.isAdmin) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
    userController.demoteToUser({ ...req, userData }, res);
});

// CHECK IF USER EXISTS BY EMAIL
router.post('/check-email', (req, res) => {
  userController.checkUserByEmail(req, res);
});

// GET USER DETAILS
// router.get("/details", (req, res) => {
//   // Provides the user's ID for the getProfile controller method
  
//       // console.log('req')
//       // console.log(req.headers.isAdmin)
//       // const userData = auth.decode(req.headers.authorization).isAdmin
//       // console.log( 'auth.decode(req.headers.authorization).isAdmin' )
//       console.log( auth.decode(req.headers.authorization).isAdmin )
  
//       userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
//   });
router.get("/details", auth.verify, (req, res) => {
  // Provides the user's ID for the getProfile controller method
  
      const userData = auth.decode(req.headers.authorization)
  
      userController.getProfile(userData).then(resultFromController => res.send(resultFromController))
  });


// Try
router.get('/', (req, res) => {
    userController.getAll(req, res);
})





module.exports = router;