const express = require('express');
const router = express.Router();
const cartController = require('../controllers/cartController');
const { verified, onlyAdmin } = require('../middleware/authMiddleware');
const auth = require('../middleware/auth');

// CART TASKS
// Add to Cart
router.post('/add-to-cart/:id', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    if (!user) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    cartController.addToCart({ ...req, user }, res);
});
// Update Book Quantities
router.put('/update-quantities/:id', auth.verify, (req, res) => {
  const user = auth.decode(req.headers.authorization);
  if (!user) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
    cartController.updateCartItem({ ...req, user }, res);
});
// Delete a Book from the cart
router.delete('/remove-book/:id', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    if (!user) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    cartController.deleteBook({ ...req, user }, res);
});
// View Cart
router.get('/view-cart', auth.verify, (req, res) => {
    const user = auth.decode(req.headers.authorization);
    if (!user) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    cartController.getCart({ ...req, user }, res);
});

module.exports = router;
