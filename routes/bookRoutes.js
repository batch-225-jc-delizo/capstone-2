const express = require('express')
const router = express.Router()
const bookController = require('../controllers/bookController')
const auth = require('../middleware/auth');
        
const { verified, onlyAdmin } = require('../middleware/authMiddleware');

// ADMIN CAPABILITIES
// Get all books, active and inactive
router.get('/', auth.verify, (req, res) => {
    // Verify if the user is an admin
    const user = auth.decode(req.headers.authorization);
    if (!user || !user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
  
    // If the user is an admin, get all books
    bookController.getBooks(req, res);
  });
// Upload new book
router.post('/add-book', auth.verify, (req, res) => {
  // Verify if the user is an admin
  const user = auth.decode(req.headers.authorization);
  if (!user || !user.isAdmin) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
    bookController.uploadBooks(req, res);
});
// Update a book
router.put('/:id', auth.verify, (req, res) => { 
    const user = auth.decode(req.headers.authorization);
    if (!user || !user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    bookController.updateBooks(req, res); 
  });
// Delete a book
router.delete('/:id', auth.verify, (req, res) => {  
  const user = auth.decode(req.headers.authorization);
  if (!user || !user.isAdmin) {
    return res.status(401).json({ message: 'Unauthorized access' });
  }
    bookController.deleteBooks(req, res); 
});
// Archive a book
router.put('/archive/:id', auth.verify, (req, res) => { 
    const user = auth.decode(req.headers.authorization);
    if (!user || !user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    bookController.archiveBooks(req, res); 
});
// Activate a book
router.put('/activate/:id', auth.verify, (req, res) => { 
    const user = auth.decode(req.headers.authorization);
    if (!user || !user.isAdmin) {
      return res.status(401).json({ message: 'Unauthorized access' });
    }
    bookController.activateBooks(req, res); 
});
// Get all active books
router.get('/activebooks', (req, res) => { 
    bookController.getActiveBooks(req, res); 
});
// Get all inactive books
router.get('/archivedbooks', onlyAdmin, (req, res) => { 
    bookController.getArchivedBooks(req, res); 
});
// Get a specific book
router.get('/specificbook/:id', (req, res) => { 
    bookController.specificBook(req, res); 
});

// Get a specific book
router.post('/search', (req, res) => { 
  bookController.searchBooks(req, res); 
});


module.exports = router;