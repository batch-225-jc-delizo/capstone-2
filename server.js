const express = require('express');
const colors = require('colors');
const dotenv = require('dotenv').config();
const cors = require('cors');
const {errorHandler} = require('./middleware/errorMiddleware')
const connectDB = require('./config/db');
const port = 5020;

// See db.js to know more
connectDB();

const app = express();

// MIDDLEWARES
app.use(cors());
app.use(express.json()) // allows you to access data sent in the request body as a JavaScript object.
app.use(express.urlencoded({ extended: false })) // for parsing data sent in the request body in the x-www-form-urlencoded format


// ROUTES!
app.use('/booknook/books', require('./routes/bookRoutes'));
app.use('/booknook/users', require('./routes/userRoutes'));
app.use('/booknook/cart', require('./routes/cartRoutes'));
app.use('/booknook/order', require('./routes/orderRoutes'));

app.use(errorHandler)

app.listen(port, () => console.log(`Server is running at port ${port}`));



