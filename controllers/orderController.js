const mongoose = require('mongoose');
const asyncHandler = require('express-async-handler')
const Order = require('../models/orderModel');
const Cart = require('../models/cartModel');
const User = require('../models/userModel');
const jwt = require('jsonwebtoken');
const Book = require('../models/bookModel');

// CREATE AN ORDER/CHECKING OUT
// Route    POST     /booknook/order/checkout
// Access   Private
module.exports.checkout = async (req, res) => {
  const { shippingAddress, paymentMode } = req.body;
  const user = req.user.id;

  try {
    // Find user's cart
    const cart = await Cart.findOne({ user });

    // Create order
    const orderItems = cart.books.map(book => {
      return {
        book: book.book._id,
        name: book.book.name,
        quantity: book.quantity,
        price: book.price
      };
    });
    const order = new Order({
      user,
      books: orderItems,
      shippingAddress,
      shippingFee: 100,
      totalPrice: cart.subtotal + 100,
      modeOfPayment: paymentMode,
    });

    // Save order to database
    await order.save();

    // Delete cart from database
    await cart.remove();

    res.json({ message: 'Order created successfully.' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};




// RETRIEVE ORDERS OF A USER
// Route    GET     /booknook/order/retrieve-a-user-order/:userId
// Access   Private (Admin Only)
module.exports.getOrdersOfUser = async (req, res) => {
    try {
      // console.log('user id:', req.user.id);
      const orders = await Order.find({ user: req.user.id });
      // console.log('orders:', orders);
      res.status(200).json(orders);
    } catch (err) {
      console.error(err.message);
      res.status(500).send('Server error');
    }
  }
  

  
  
  
  
  



// RETRIEVE ALL ORDERS
// Route    GET     /booknook/order/get-orders
// Access   Private (Admin Only)
module.exports.getOrdersOfAllUsers = asyncHandler(async (req, res) => {
    const orders = await Order.find();
    res.status(200).json(orders);
});


// DELETE AN ORDER
// Route    DELETE     /booknook/order/delete-order
// Access   Private (Admin Only)
module.exports.deleteOrder = asyncHandler(async (req, res, next) => {
    // get the order id from the request params
    const orderId = req.params.id;

    // find the order by id
    const order = await Order.findById(orderId);
    // check if the order is not found
    if (!order) {
        return res.status(404).json({ error: 'Order not found' });
    }

    // delete the order
    await order.remove();

    // return a success message
    res.status(200).json({
        message: `Order with ID ${req.params.id} deleted succesfully!`
    });
});


// DELIVER AN ORDER
// Route    PUT     /booknook/order/order-delivered/:id
// Access   Private (Admin Only)
module.exports.orderDeliver = async (req, res) => {
  const { id } = req.params;
  const { user } = req;
  
  try {
    const order = await Order.findById(id);
    
    if (!order) {
      return res.status(404).json({ message: 'Order not found' });
    }
    
    if (order.isDelivered) {
      // Order is already delivered, update isDelivered to false
      order.isDelivered = false;
    } else {
      // Order is not delivered, update isDelivered to true
      order.isDelivered = true;
    }
    
    order.updatedBy = user.id;
    
    const updatedOrder = await order.save();
    
    res.json(updatedOrder);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Internal server error' });
  }
};

// Get all delivered orders
module.exports.delivered = async (req, res) => {
  try {
    const orders = await Order.find({ isDelivered: true });
    res.status(200).json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};

// Get all not delivered orders
module.exports.notDelivered = async (req, res) => {
  try {
    const orders = await Order.find({ isDelivered: false });
    res.status(200).json(orders);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
};