const asyncHandler = require('express-async-handler')
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const User = require('../models/userModel')
const Cart = require('../models/cartModel');
const auth = require('../middleware/auth')


// REGISTER NEW USER
// Route    POST     /booknook/users
// Access   Public
module.exports.registerUser = asyncHandler(async (req, res) => {
    const { name, email, mobileNo, password } = req.body;
    
    // Ensure all required fields are filled
    if (!name || !email || !mobileNo || !password) {
        throw new Error('Please provide all the required information: name, email, mobile number and password');
    }

    // Ensure that the email does not exist
    const user = await User.findOne({ email });
    if (user) {
        throw new Error('User already exists');
    }

    // Hash the password!!!
    const hashedPassword = await bcrypt.hash(password, 10);
    const newUser = new User({
        name,
        email,
        mobileNo,
        password: hashedPassword
    });
    
    try {
        // Save the new user to the database
        await newUser.save();
        // Sign and return a JWT
        const token = jwt.sign({ userId: newUser._id }, process.env.SECRET, { expiresIn: '1d' });
        res.status(201).json({ 
            message: `Welcome ${newUser.name}!`,
            user: { id: newUser._id, name: newUser.name, email: newUser.email },
            token});
    } catch(err) {
        // Return a server error if something went wrong while saving the new user
        res.status(500).json({ error: "Error creating user: " + err }); //sometimes the error doesn't come out lol
    }
});


// LOGIN NEW USER
// Route    POST     /booknook/users/login
// Access   Public
// module.exports.loginUser = async (req, res) => {
//     const { email, password } = req.body;

//     // Check for user email in database
//     const user = await User.findOne({ email });

//     // Compare passwords
//     if (user && (await bcrypt.compare(password, user.password))) {
//         // Generate token if passwords match
//         const token = generateToken(user._id);
//         return {
//             message: 'Logged In Successfully!',
//             _id: user.id,
//             name: user.name,
//             email: user.email,
//             isAdmin: user.isAdmin,
//             token: token
//         };
//     } else {
//         // Throw error if passwords do not match or user is not found
//         throw new Error('Invalid User Credentials');
//     }
// };

module.exports.loginUser = (data) => {

    return User.findOne({email : data.email}).then(result => {

        if(result == null ){

            return {
                message: "Not found in our database"
            }

        } else {

            // The "compareSync" method is used to compare a non encrypted password from the login form to the encrypted password retrieved from the database and returns "true" or "false" value depending on the result
            const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);


            if (isPasswordCorrect) {

                return {access : auth.createAccessToken(result)}

            } else {

                // if password doesn't match
                return {
                    message: "password was incorrect"
                }
            };

        };

    });
};



// RETRIEVE ANY USER DETAILS 
// Route    POST    /booknook/users/whosthisuser/:userId
// Access   Private (Admin Only)
module.exports.getUser = asyncHandler(async (req, res) => {
    const user = await User.findById(req.params.id);
  
    if (!user) {
      return res.status(404).json({
        success: false,
        message: `User with ID ${req.params.id} not found`,
      });
    }
  
    res.status(200).json(user);
  });
  


// RETRIEVE ALL USERS
// Route    POST    /booknook/users/all
// Access   Private (Admin Only)
module.exports.getAllUsers = asyncHandler(async (req, res) => {
    const users = await User.find({});
    res.status(200).json(users);
  });



// RETRIEVE A USER'S OWN DETAILS
// Route    POST    /booknook/users/me
// Access   Private (Admin and User)
    
module.exports.meUser = async (userData) => {
    return User.findOne({_id : userData.id}).then(result => {
        if (result == null){
            return false
        } else {
            return {
                id: userData._id,
                name: userData.name,
                email: userData.email,
                mobileNo: userData.mobileNo,
                createdOn: userData.createdOn,
                updatedOn: userData.updatedOn,
                isAdmin: userData.isAdmin
            }
        }
    });
};
  
  

// PROMOTE USER TO ADMIN
// Route    PUT   /booknook/users/makeadmin/:userId
// Access   Private (Admin Only)
module.exports.promoteToAdmin = asyncHandler(async (req, res) => {
    // Get the user id from the request params
    const userId = req.params.id;
    // Find the user by their id
    const user = await User.findById(userId);
    // If the user is not found, return a 404 status with an error message
    if (!user) {
      res.status(404).json({ error: 'User not found' });
      return;
    }
  
    // Get the super admin user id from the request data
    const superAdminUserId = req.userData.id;
    console.log(superAdminUserId)
  
    // Check if the user making the request is the super admin
    if (superAdminUserId !== '63d27439c9998d31dec04950') {
      res.status(401).json({ error: 'Unauthorized access' });
      return;
    }
  
    // Set the user's isAdmin property to true
    user.isAdmin = true;
    try {
      // Save the user's updated data
      await user.save();
      // Return a 200 status with a message and the updated user data
      res.status(200).json({
        message: 'User promoted to admin successfully!',
        data: {
          id: user._id,
          name: user.name,
          email: user.email,
          createdOn: user.createdOn,
          updatedOn: user.updatedOn,
          isAdmin: user.isAdmin,
        },
      });
    } catch (err) {
      // If there is an error saving the user's data, return a 500 status with an error message
      res.status(500).json({
        error: 'Error promoting user to admin: ' + err,
      });
    }
  });
  


// DEMOTE AN ADMIN TO USER
// Route    PUT   /booknook/users/makeuser/:userId
// Access   Private (Admin Only)
module.exports.demoteToUser = asyncHandler(async (req, res) => {
    // Get the user id from the request params
    const userParamId = req.params.id;
    // Find the user by their id
    const user = await User.findById(userParamId);
    // If the user is not found, return a 404 status with an error message
    if (!user) {
      res.status(404).json({ error: 'User not found' });
      return;
    }
  
    const adminId = req.userData.id;
    console.log(adminId)
  
    // If the admin is not authorized to demote another admin to user, return a 401 status with an error message
    if (adminId !== '63d27439c9998d31dec04950') {
      res.status(401).json({ error: 'Unauthorized access Controller' });
      return;
    }
  
    // Set the user's isAdmin property to false
    user.isAdmin = false;
    try {
      // Save the user's updated data
      await user.save();
      // Return a 200 status with a message and the updated user data
      res.status(200).json({
        message: 'Admin demoted to user successfully!',
        data: {
          id: user._id,
          name: user.name,
          email: user.email,
          createdOn: user.createdOn,
          updatedOn: user.updatedOn,
          isAdmin: user.isAdmin,
        },
      });
    } catch (err) {
      // If there is an error saving the user's data, return a 500 status with an error message
      res.status(500).json({
        error: 'Error demoting admin to user: ' + err,
      });
    }
  });
  


// Get User Details
// Route    PATCH   /booknook/users/details
// Access   Public
// module.exports.getProfile = async (userData) => {
//   try {
//     let result = await User.findOne({ email: userData.email });
//     console.log(userData)
//     if (result == null) {
//       return { success: false, message: "User not found", status: 404 };
//     } else {
//       result.password = "";
//       return { success: true, data: result };
//     }
//   } catch (error) {
//     return { success: false, message: "Server error", status: 500 };
//   }
// };
// module.exports.getProfile = (userData) => {
//     console.log(userData)
//     console.log(req.headers.authorization)
//     return User.findOne(userData.id).then(result => {
//         if (result == null){
//             return false
//         } else {
//             result.password = ""
//             return result
//         }
//     });
// };
// Retrieve User Details
module.exports.getProfile = (userData) => {
    return User.findOne({_id : userData.id}).then(result => {
        if (result == null){
            return false
        } else {
            result.password = ""
            return result
        }
    });
};
  


// TRY
module.exports.getAll = async (req, res) => {
    let result = await User.find({});
    res.send(result)
}

// CHECK IF USER EXISTS BY EMAIL
// Route    POST    /booknook/users/check-email
// Access   Private (Admin Only)
module.exports.checkUserByEmail = asyncHandler(async (req, res) => {
    const { email } = req.body;
    // Find user by email in the database
    const user = await User.findOne({ email });
    // If user is not found, return false
    if (!user) {
        return res.status(200).json({ 
            success: false,
            message: `User not found with email ${email}`
        });
    }
    // If user is found, return true
    res.status(200).json({ 
        success: true,
        message: `User found with email ${email}`
    });
});




// GENERATE TOKEN
const generateToken = (userId) => {
    return jwt.sign({ userId }, process.env.SECRET, { expiresIn: '1d' });
}

