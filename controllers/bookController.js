const asyncHandler = require('express-async-handler')
const Book = require('../models/bookModel')


// GET BOOKS
// Route    GET     /booknook/books
// Access   Private
module.exports.getBooks = asyncHandler( async (req, res) => {
    const books = await Book.find()

    res.status(200).json(books)
})


// UPLOAD BOOKS
// Route    POST     /booknook/books/add-book
// Access   Private
module.exports.uploadBooks = asyncHandler(async (req, res) => {
  // Check if the request body is empty
  if (!req.body) {
    // Return a 400 status code if the request body is empty
    res.status(400)
    // Throw an error with a message
    throw new Error('Please complete the details.')
  }

  // Create a new book using the data from the request body
  const book = await Book.create({
    name: req.body.name,
    description: req.body.description,
    author: req.body.author,
    genre: req.body.genre,
    price: req.body.price,
    image: req.body.image // add the image field
  })

  // Return a 200 status code and the created book in the response
  res.status(200).json({ message: `The book ${book.name} is uploaded!`, book })
})



// UPDATE BOOKS
// Route    PUT     /booknook/books/:id
// Access   Private
module.exports.updateBooks = asyncHandler(async (req, res) => {
  // Find a book with the id specified in the request parameters
  const book = await Book.findById(req.params.id)

  // If the book is not found, return a 404 status code and throw an error
  if (!book) {
    res.status(404)
    throw new Error(`Book with id ${req.params.id} was not found`)
  }

  // Update the book's properties with the data from the request body
  book.name = req.body.name || book.name
  book.description = req.body.description || book.description
  book.author = req.body.author || book.author
  book.genre = req.body.genre || book.genre
  book.price = req.body.price || book.price
  book.image = req.body.image || book.image
  book.updatedOn = Date.now()

  // Save the updated book to the database
  const updatedBook = await book.save()

  // Return a 200 status code, a success message, and the updated book in the response
  res.status(200).json({ message: `The book ${book.name} is updated!`, updatedBook })
})



// DELETE BOOKS
// Route    DELETE     /booknook/books/:id
// Access   Private
module.exports.deleteBooks = asyncHandler( async (req, res) => {
    // Find and delete a book with the id specified in the request parameters
    const book = await Book.findByIdAndDelete(req.params.id)
    
    // If the book is not found, throw an error
    if (!book) {
      throw new Error(`Book with id ${req.params.id} was not found`)
    }
    
    // Return a 200 status code and a success message in the response
    res.status(200).json({ message: `${book.name} was deleted` })
  })


// ARCHIVE BOOKS
// Route    PUT     /booknook/books/archive/:id
// Access   Private
module.exports.archiveBooks = asyncHandler(async (req, res) => {
    // Find a book with the id specified in the request parameters
    const book = await Book.findById(req.params.id)
    
    // If the book is not found, return a 404 error message in the response
    if (!book) {
      return res.status(404).json({ message: `Book with id ${req.params.id} was not found` })
    }
    
    // If the book is already inactive, return a 400 error message in the response
    if (!book.isActive) {
      return res.status(400).json({ message: `The book ${book.name} is already inactive` })
    }
    
    // Set the book's isActive property to false
    book.isActive = false
    
    // Update the book's updatedOn property to the current time
    book.updatedOn = Date.now()
    
    // Save the updated book
    const updatedBook = await book.save()
    
    // Return a 200 status code and a success message in the response
    return res.status(200).json({ message: `The book ${book.name} is now archived.`, "updatedOn": book.updatedOn })
  })


// ACTIVATE BOOKS
// Route    PUT     /booknook/books/activate/:id
// Access   Private
module.exports.activateBooks = asyncHandler(async (req, res) => {
    // Find book by id from request parameters
    const book = await Book.findById(req.params.id)

    // If book is not found, return a 404 error with message
    if (!book) {
        return res.status(404).json({ message: `Book with id ${req.params.id} was not found` })
    }

    // If book is already active, return a 400 error with message
    if (book.isActive) {
        return res.status(400).json({ message: `The book ${book.name} is already active` })
    }

    // Set the book to active and update the updatedOn field
    book.isActive = true
    book.updatedOn = Date.now()
    const updatedBook = await book.save()

    // Return a 200 response with message and updatedOn field
    return res.status(200).json({ message: `The book ${book.name} is now active`, "updatedOn": book.updatedOn })
})


// RETRIEVE ALL ACTIVE BOOKS
// Route    GET     /booknook/books/activebooks
// Access   PRIVATE
module.exports.getActiveBooks = asyncHandler(async (req, res) => {
    // Find all books that have the "isActive" property set to true
    const activeBooks = await Book.find({ isActive: true });

    // Return a success status code and the found active books
    res.status(200).json(activeBooks);
});



// RETRIEVE ALL INACTIVE BOOKS
// Route    GET     /booknook/books/archivedbooks
// Access   PRIVATE
module.exports.getArchivedBooks = asyncHandler(async (req, res) => {
    // Find all books where isActive is false
    const archivedBooks = await Book.find({ isActive: false });

    // Respond with a 200 status code and the archived books in a JSON object
    res.status(200).json({ message: `Below are the books in Archive`, archivedBooks})
});



// GET A SPECIFIC BOOK
// Route    GET     /booknook/books/specificbook
// Access   PRIVATE
module.exports.specificBook = async (req, res) => {
    // Destructuring the id from request parameters
    const { id } = req.params;

    try {
      // Finding book in the database by id
      const book = await Book.findById(id);

      // Checking if book is not found in the database
      if (!book) {
        return res.status(404).json({ message: `There is no book in the database with id of: ${id}` });
      }

      // Returning the found book in the response
      res.status(200).json(book);
    } catch (error) {
      // Return 500 status in case of any error
      res.status(500).json({ message: "Server Error" });
    }
  };

  

// SEARCH FOR A BOOK BY NAME
// Route    POST    /booknook/books/search
// Access   PRIVATE
module.exports.searchBooks = async (req, res) => {
  // Destructuring the name from request body
  const { name } = req.body;

  try {
    // Finding books in the database that match the name using a regular expression
    const books = await Book.find({ name: { $regex: new RegExp(name, "i") } });

    // Checking if books are not found in the database
    if (!books || books.length === 0) {
      return res.status(404).json({ message: `There are no books in the database with a name containing '${name}'` });
    }

    // Returning the found books in the response
    res.status(200).json(books);
  } catch (error) {
    // Return 500 status in case of any error
    res.status(500).json({ message: "Server Error" });
  }
};
