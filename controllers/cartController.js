const asyncHandler = require('express-async-handler')
const Cart = require("../models/cartModel");
const Book = require("../models/bookModel");
const jwt = require('jsonwebtoken');


// ADD TO CART
// Route    POST     /booknook/cart/add-to-cart
// Access   Private (Users and Admins)
module.exports.addToCart = asyncHandler(async (req, res) => {
  try {
    const bookId = req.params.id;
    const quantity = req.body.quantity;

    const userId = req.user.id;
    const cart = await Cart.findOne({ user: userId });

    if (!cart) {
      // create a new cart for the user if one does not exist
      const book = await Book.findById(bookId);
      const newCart = new Cart({
        user: userId,
        books: [{ book: bookId, quantity: quantity }],
        subtotal: quantity * book.price
      });
      await newCart.save();
      return res.status(200).json({ message: "Book added to cart" });
    } else {
      // check if the book already exists in the user's cart
      const bookIndex = cart.books.findIndex(book => book.book == bookId);
      if (bookIndex >= 0) {
        // if book already exists, update the quantity
        cart.books[bookIndex].quantity += quantity;
      } else {
        // if book doesn't exist, add it to the user's cart
        const book = await Book.findById(bookId);
        cart.books.push({ book: bookId, quantity: quantity });
      }
      const book = await Book.findById(bookId);
      cart.subtotal += quantity * book.price;
      await cart.save();
      return res.status(200).json({ message: "Book added to cart" });
    }
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
});









// UPDATE THE BOOK QUANTITIES
// Route    POST     /booknook/cart/update-quantities
// Access   Private (Users and Admins)
module.exports.updateCartItem = asyncHandler(async (req, res) => {
  try {
    const userId = req.user.id;
    const cart = await Cart.findOne({ user: userId });
    const bookId = req.params.id;
    const quantity = req.body.quantity;
    // console.log("Before update: ", cart.subtotal);

    const book = await Book.findById(bookId);
    // console.log("Book: ", book);
    const bookPrice = book.price;
    const bookSubtotal = bookPrice * quantity;
    // console.log("Book price: ", bookPrice);
    // console.log("Book subtotal: ", bookSubtotal);

    const bookIndex = cart.books.findIndex(book => book.book._id == bookId);
    // console.log("Book index: ", bookIndex);

    if (bookIndex >= 0) {
      const oldQuantity = cart.books[bookIndex].quantity;
      const oldSubtotal = oldQuantity * bookPrice;
      cart.subtotal -= oldSubtotal;
      if (quantity === 0) {
        cart.books.splice(bookIndex, 1);
      } else {
        cart.books[bookIndex].quantity = quantity;
        // console.log("Quantity updated: ", quantity);
        const updatedQuantity = cart.books[bookIndex].quantity;
        const updatedSubtotal = updatedQuantity * bookPrice;
        cart.subtotal += updatedSubtotal;
      }
    }

    // console.log("Subtotal after updating book quantity: ", cart.subtotal);

    await cart.save();
    // console.log("After saving cart: ", cart.subtotal);
    return res.status(200).json({ message: "Cart item updated" });
  } catch (error) {
    console.log(error);
    return res.status(500).json({ message: "Internal Server Error" });
  }
});









// DELETE A BOOK FROM THE CART
// Route    DELETE     /booknook/cart/remove-book
// Access   Private (Users and Admins)
module.exports.deleteBook = asyncHandler(async (req, res, next) => {
  // Get the user ID from the decoded token
  const userId = req.user.id;
  // Get the book ID from the request parameters
  const bookId = req.params.id;
  // Find the cart belonging to the user
  let cart = await Cart.findOne({ user: userId });
  // Return error if cart is not found
  if (!cart) {
    return res.status(404).json({ error: 'Cart not found' });
  }
  // Find the book in the cart and remove it
  cart.books = cart.books.filter(book => book.book.toString() !== bookId);
  // Recalculate the cart total
  cart.total = cart.books.reduce((acc, book) => acc + book.subtotal, 0);
  // If the cart has no books left, delete it
  if (cart.books.length === 0) {
    await Cart.findByIdAndDelete(cart._id);
    return res.status(200).json({ message: 'Book removed from cart and cart deleted' });
  }
  // Otherwise, save the updated cart to the database
  await cart.save();
  res.status(200).json({ message: 'Book removed from cart' });
});




// VIEW CART
// Route    GET     /booknook/cart/view-cart
// Access   Private (Users and Admins)
module.exports.getCart = asyncHandler(async (req, res, next) => {
  // Get the user ID from the decoded token
  const userId = req.user.id;
  // Find the cart belonging to the user
  let cart = await Cart.findOne({ user: userId }).populate('books.book');
  // Return an empty cart object if no cart is found
  if (!cart) {
    cart = { books: [], total: 0 };
  } else {
    // calculate the total of the cart
    cart.total = cart.books.reduce((acc, book) => acc + book.subtotal, 0);
  }
  // Return the cart
  res.status(200).json(cart);
});
