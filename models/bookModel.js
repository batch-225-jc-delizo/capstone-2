const mongoose = require('mongoose');

const bookSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, "Name is required."]
    },
    description: {
        type: String,
        required: [true, "Description is required."]
    },
    author: {
        type: String,
        required: [true, "Author is required."]
    },
    genre: {
        type: String,
        required: [true, "Genre is required."]
    },
    price: {
        type: Number,
        required: [true, "Price is required."]
    },
    image: {
        type: String,
        required: [true, "Image is required."]
    },
    createdOn: {
        type: Date,
        default: Date.now
    },
    updatedOn: {
        type: Date,
        default: Date.now
    },
    isActive: {
        type: Boolean,
        default: true
    }
});

module.exports = mongoose.model('Book', bookSchema);
