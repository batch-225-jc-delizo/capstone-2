const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    name: {
        type: String,
        required: [true, 'Please add a name.']
    },
    email: {
        type: String,
        required: [true, 'Please add an email.']
    },
    mobileNo: {
        type: String,
        required: [true, 'Please add a mobile number.']
    },
    password: {
        type: String,
        required: [true, 'Please add a password.']
    },
    createdOn:{
        type: Date,
        default: Date.now
    },
    updatedOn:{
        type: Date,
        default: Date.now
    },
    isAdmin: {
		type: Boolean, 
		default: false
    },
})

module.exports = mongoose.model('User', userSchema)