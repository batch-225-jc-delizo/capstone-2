const mongoose = require('mongoose');

const orderSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Please add a user.']
    },
    books: [{
        book: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Book',
            required: [true, 'Please add a book.']
        },
        quantity: {
            type: Number,
            required: [true, 'Please specify the quantity.'],
            default: 1
        }
    }],
    shippingAddress: {
        type: String,
        required: [true, 'Please add a shipping address.'],
    },
    shippingFee: {
        type: Number,
        default: 100
    },
    totalPrice: {
        type: Number,
        required: [true, 'Please specify a total price.']
    },
    modeOfPayment: {
        type: String,
        required: [true, 'Please specify a mode of payment.']
    },
    isDelivered: {
        type: Boolean,
        default: false
    },
    createdOn:{
        type: Date,
        default: Date.now
    },
    updatedOn:{
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Order', orderSchema);
